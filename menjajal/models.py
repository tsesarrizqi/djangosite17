from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
class Role(models.Model):
    name = models.CharField(max_length=30)
	
    def __str__(self):
        return str(self.id) + self.name;
class Admin(models.Model):
    nip = models.CharField(max_length=18, unique=True)
    role_id = models.ForeignKey(Role, to_field='id')
    user = models.OneToOneField(User, primary_key=True)
    token = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)
class Level(models.Model):
    name = models.CharField(max_length=30)
class Faculty(models.Model):
    name = models.CharField(max_length=50)
class Status(models.Model):
    name = models.CharField(max_length=100)
class Regulation(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)
class RegulationQuestion(models.Model):
    rule_id = models.ForeignKey(Regulation, to_field='id')
    creator_nip = models.ForeignKey(Admin, to_field='nip')
    question = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)
class CalonYangDiusulkan(models.Model):
    nip = models.CharField(max_length=18, unique=True)
    name = models.CharField(max_length=50)
    birthdate = models.DateTimeField(default=datetime.now, blank=True)
    birthplace = models.CharField(max_length=30)
    current_level = models.ForeignKey(Level, to_field='id')
    email = models.CharField(max_length=50)
    faculty = models.ForeignKey(Faculty, to_field='id')
    gender = models.CharField(max_length=1)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)
class Application(models.Model):
    nip = models.ForeignKey(CalonYangDiusulkan, to_field='nip')
    status_id = models.ForeignKey(Status, to_field='id')
    rule_id = models.ForeignKey(Regulation, to_field='id')
    destination_level = models.ForeignKey(Level, to_field='id')
    application_bundle_path = models.TextField()
    acceptance_letter_file_path = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    updated_at = models.DateTimeField(default=datetime.now, blank=True)
class EducationHistory(models.Model):
    cyd_id = models.ForeignKey(CalonYangDiusulkan, to_field='id')
    name = models.TextField()
    order = models.IntegerField()
class ApplicationHistory(models.Model):
    cyd_id = models.ForeignKey(CalonYangDiusulkan, to_field='id')
    status_id = models.ForeignKey(Status, to_field='id')
    description = models.TextField()
    revision_number = models.IntegerField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
class Record(models.Model):
    application_history_id = models.ForeignKey(ApplicationHistory, to_field='id')
    rule_question_id = models.ForeignKey(RegulationQuestion, to_field='id')
    record = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
class StatusChangeRecord(models.Model):
    cyd_id = models.ForeignKey(CalonYangDiusulkan, to_field='id')
    status_id = models.ForeignKey(Status, to_field='id')
    description = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)